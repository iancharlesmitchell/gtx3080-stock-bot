from stock_watch import StockWatch

def main():
    client = StockWatch()
    client.run_check_stock_routine()

if __name__ == "__main__":
    main()    