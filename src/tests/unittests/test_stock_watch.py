import unittest
from pathlib import Path
from stock_watch import StockWatch
from unittest.mock import patch, MagicMock

CURRENT_DIRECTORY = Path(__file__).resolve().parent
DEFAULT_CONFIG_PATH = CURRENT_DIRECTORY / "../../../config/test_config.json"


class TestStockWatch(unittest.TestCase):
    def setUp(self):
        self.test_config = [
                {
                    "Site": "Nvidia.com",
                    "URL": "https://www.nvidia.com/en-us/shop/geforce/?page=1&limit=9&locale=en-us&gpu=RTX%203080&manufacturer=NVIDIA&manufacturer_filter=NVIDIA~1,ACER~0,ALIENWARE~0,ASUS~3,DELL~0,EVGA~3,GIGABYTE~2,HP~0,LENOVO~0,LG~0,MSI~3,PNY~2,RAZER~0,ZOTAC~2",
                    "Element": "#resultsDiv > div > div > div.buy-col-lg > div:nth-child(1) > div.buy > a",
                    "Out_Of_Stock_Text": "Out Of Stock",
                    "SMS": ["7817715265"]
                }
            ]
        self.test_config_path = DEFAULT_CONFIG_PATH
        self.stock_watch = StockWatch(self.test_config_path)
        self.test_url = self.test_config[0]["URL"]
        self.test_site = self.test_config[0]["Site"]
        self.test_receivers = self.test_config[0]["SMS"]  
        self.test_element = self.test_config[0]["Element"]
        self.out_of_stock_text = self.test_config[0]["Out_Of_Stock_Text"]

    def test_is_instance(self):
        self.assertIsInstance(self.stock_watch, StockWatch)     

    def test_read_config(self):
        self.assertEqual(self.stock_watch.config, self.test_config)       

    def test_check_stock(self):
        self.assertTrue(self.stock_watch.check_stock(self.test_url, self.test_element, self.out_of_stock_text))   

    @patch("stock_watch.StockWatch._send_sms", autospec=True)
    def test_notify_users_negative(self, mock_method):
        with self.assertRaises(ValueError):
            self.stock_watch.notify_users(self.test_site, self.test_url, self.test_receivers, "email")

    @patch("stock_watch.Client.messages", autospec=True)
    def test_send_sms(self, mock_method):
        messages = self.stock_watch._send_sms(self.test_site, self.test_url, self.test_receivers)
        self.assertEqual(len(messages), len(self.test_receivers))

if __name__ == "__main__":
    unittest.main()        