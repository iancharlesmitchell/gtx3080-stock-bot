from pathlib import Path
import json
from typing import List, Dict, Union
from requests_html import HTMLSession # type: ignore
import logging
from twilio.rest import Client # type: ignore
import os
import sys

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

handler = logging.StreamHandler(sys.stdout)
logger.addHandler(handler)

CURRENT_DIRECTORY = Path(__file__).resolve().parent
DEFAULT_CONFIG_PATH = CURRENT_DIRECTORY / "../../config/config.json"

class StockWatch(object):
    def __init__(self, config_path: Union[str, Path] = DEFAULT_CONFIG_PATH, alert="text"):
        self.config_path = config_path
        self.config = self._read_config(self.config_path)

    def _read_config(self, config_path: Union[str, Path]) -> List[Dict]:
        with open(config_path) as f:
            return json.loads(f.read())  

    def check_stock(self, url: str, search_element: str, out_of_stock_text: str = "Out Of Stock") -> bool:
        logger.info(f"Checking URL: {url}")
        session = HTMLSession()
        html = session.get(url)
        html.html.render(timeout=120)
        logger.info(f"Searching for element: {search_element}")
        elements = html.html.find(selector=search_element)
        logger.info(f"Number of Elements Found: {len(elements)}")
        logger.info(f"Elements: {elements}")
        out_of_stock = False
        for element in elements:
            logger.info(f"Element Text: {element.text}")
            if element.text == out_of_stock_text:
                out_of_stock = True
        logger.info(f"Out Of Stock: {out_of_stock}")       
        return out_of_stock        

    def notify_users(self, site: str, url: str, receivers: List, method: str = "sms"):
        if method == "sms":
            self._send_sms(site, url, receivers)
        else:
            raise ValueError(f"Method: {method} not allowed.")    

    def _send_sms(self, site: str, url: str, receivers: List) -> List:
        account_sid = os.getenv("ACCOUNT_SID")
        auth_token = os.getenv("AUTH_TOKEN")
        from_number = os.getenv("FROM_NUMBER")
        client = Client(account_sid, auth_token)
        message_confirmations = []
        for receiver in receivers:
            logger.info(f"Sending in stock message to: {receiver} for {site}.")
            message = client.messages \
                        .create(
                            body = f"Stock available on {site}. {url}",
                            from_ = from_number,
                            to = receiver
                        )                 
            message_confirmations.append(message.sid)
        return message_confirmations                                               
                        
    def run_check_stock_routine(self):
        for page in self.config:
            site = page["Site"]
            url = page['URL']
            element = page['Element']
            out_of_stock_text = page["Out_Of_Stock_Text"]
            receivers = page['SMS']
            out_of_stock = self.check_stock(url, element, out_of_stock_text)
            if not out_of_stock:
                self.notify_users(site, url, receivers, method="sms")